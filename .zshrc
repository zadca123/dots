autoload -U colors && colors    # Load colors

# git prompt
autoload -Uz vcs_info
precmd_vcs_info() { vcs_info; }
precmd_functions+=(precmd_vcs_info)
setopt prompt_subst
# shellcheck disable=SC2016
RPROMPT='%F{2}${vcs_info_msg_0_}%f'
zstyle ':vcs_info:git:*' formats '%b'

# k8s prompt
get_k8s_info() {
    local k8s_context k8s_namespace
    k8s_context=$(kubectl config current-context 2>/dev/null)
    k8s_namespace=$(kubectl config view --minify --output 'jsonpath={..namespace}' 2>/dev/null)

    # If both context and namespace are available, display them
    if [[ -n $k8s_context ]]; then

        echo "[${k8s_context}:${k8s_namespace}]" # the brachet type matters!
    fi
}
# shellcheck disable=SC2016
RPROMPT+=' %F{yellow}$(get_k8s_info)%f'

NEWLINE=$'\n'
# shellcheck disable=SC2034
PROMPT="%B%F{1}[%F{6}%n%F{7}@%F{4}%m%F{7}:%F{5}%80<..<%~%F{1}]%F{2}${NEWLINE}$%f%b "

# options: history
setopt hist_ignore_all_dups
setopt hist_ignore_space
setopt hist_verify
setopt inc_append_history

# options: misc
setopt autocd       # Automatically cd into typed directory.
setopt interactive_comments # allow comments in interactive shell

# completions, https://thevaluable.dev/zsh-completion-guide-examples/
zstyle ':completion:*' use-ip true
zstyle ':completion:*' use-cache on
zstyle ':completion:*' cache-path "$XDG_CACHE_HOME/zsh/.zcompcache"
zstyle ':completion:*' completer _extensions _complete _approximate
zstyle ':completion:*:*:*:*:descriptions' format '%F{green}-- %d --%f'
zstyle ':completion:*:*:*:*:corrections' format '%F{yellow}!- %d (errors: %e) -!%f'
zstyle ':completion:*:messages' format ' %F{purple} -- %d --%f'
zstyle ':completion:*:warnings' format ' %F{red}-- no matches found --%f'
zstyle ':completion:*' group-name ''
# zstyle ':completion:*:*:-command-:*:*' group-order alias builtins functions commands
zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS} # Colorful Completion List
zstyle ':completion:*' squeeze-slashes true
# zstyle ':completion:*' file-sort access reverse
# zstyle ':completion:*' file-list all # ls -l

# Basic auto/tab complete:
autoload -U compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit
_comp_options+=(globdots)       # Include hidden files.


# https://forum.endeavouros.com/t/tip-better-url-pasting-in-zsh/6962
# First load zsh-autosuggestions
source /usr/share/zsh-autosuggestions/zsh-autosuggestions.zsh
ZSH_AUTOSUGGEST_BUFFER_MAX_SIZE=20
# other ZSH_AUTOSUGGEST config goes here

# Then load url-quote-magic and bracketed-paste-magic as above
autoload -U url-quote-magic bracketed-paste-magic
zle -N self-insert url-quote-magic
zle -N bracketed-paste bracketed-paste-magic

# Now the fix, setup these two hooks:
pasteinit() {
    OLD_SELF_INSERT=${${(s.:.)widgets[self-insert]}[2,3]}
    zle -N self-insert url-quote-magic
}
pastefinish() {
    zle -N self-insert $OLD_SELF_INSERT
}
zstyle :bracketed-paste-magic paste-init pasteinit
zstyle :bracketed-paste-magic paste-finish pastefinish

# and finally, make sure zsh-autosuggestions does not interfere with it:
ZSH_AUTOSUGGEST_CLEAR_WIDGETS+=(bracketed-paste)

# emacs keybindings mode
bindkey -e
x-copy-region-as-kill () {
    zle copy-region-as-kill
    print -rn $CUTBUFFER | xsel -i -b
}
zle -N x-copy-region-as-kill
x-kill-region () {
    zle kill-region
    print -rn $CUTBUFFER | xsel -i -b
}
zle -N x-kill-region
x-yank () {
    CUTBUFFER=$(xsel -o -b </dev/null)
    zle yank
}
zle -N x-yank
go-back() {
    cd ..
    echo ""
    zle reset-prompt
}
zle -N go-back
bindkey "^\^" go-back
bindkey -e '\ew' x-copy-region-as-kill
bindkey -e '^W' x-kill-region
bindkey -e '^Y' x-yank
bindkey -s '^x' 'fzf_cmd\n' # function inside ~/.rc.d/environment_variables
bindkey -s '^t' 'tmux new-session -As0\n'
bindkey -s '^v' 'bg && disown\n'
bindkey '^[[1;5D' backward-word
bindkey '^[[1;5C' forward-word

# alt + key (alt == \e)
bindkey -s '\ep' 'pass-fzy.sh\n'

source <(cd "$RC_DIRECTORY" && cat -- *)

# Load syntax highlighting; should be last.
source "$(find /usr/share/zsh* -mount -type f -name '*-syntax-highlighting*.zsh')" || echo "install zsh-syntax-highlighting"
