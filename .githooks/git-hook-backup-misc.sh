#!/usr/bin/env bash

crontab -l > .misc/crontab
cp /etc/fstab .misc/
cp /etc/dnf/dnf.conf .misc/

git add .misc/*
