#!/usr/bin/env bash

# partial_path=".config/transmission-daemon/torrents"
# home_path="${HOME}/${partial_path}"
# dotfiles_path="${HOME}/.dotfiles/${partial_path}"
# archive="transmission-daemon.backup.tar.gz"

# cd "$home_path"
# tar caf "$archive" *.torrent
# mv "$archive" "$dotfiles_path"

# cd "$dotfiles_path"
# git add "$archive"


archive="transmission-daemon.backup.tar.gz"
DIR="$PWD"
cd ~/.config/
tar caf "$archive" ./transmission-daemon
mv "$archive" "${DIR}/.misc/"
cd "${DIR}/.misc"
git add "$archive"
