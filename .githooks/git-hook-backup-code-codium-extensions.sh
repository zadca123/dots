#!/usr/bin/env bash


DOTFILES="${HOME}/.dotfiles"

code_exts=".config/Code/User/vscode-extensions.txt"
codium_exts=".config/VSCodium/User/vscodium-extensions.txt"

cd "$DOTFILES"

command -v codium && codium --list-extensions > "${codium_exts}"
command -v code && code --list-extensions > "${code_exts}"

git add "$codium_exts"
git add "$code_exts"
