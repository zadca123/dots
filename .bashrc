#
# ~/.bashrc
#

# prompt
FMT_BOLD="\[\e[1m\]"
FMT_DIM="\[\e[2m\]"
FMT_RESET="\[\e[0m\]"
FMT_UNBOLD="\[\e[22m\]"
FMT_UNDIM="\[\e[22m\]"
FG_BLACK="\[\e[30m\]"
FG_BLUE="\[\e[34m\]"
FG_CYAN="\[\e[36m\]"
FG_GREEN="\[\e[32m\]"
FG_GREY="\[\e[37m\]"
FG_MAGENTA="\[\e[35m\]"
FG_RED="\[\e[31m\]"
FG_WHITE="\[\e[97m\]"
BG_BLACK="\[\e[40m\]"
BG_BLUE="\[\e[44m\]"
BG_CYAN="\[\e[46m\]"
BG_GREEN="\[\e[42m\]"
BG_MAGENTA="\[\e[45m\]"

# PS1+=" \$(find . -mindepth 1 -maxdepth 1 -type d | wc -l) " # print number of folders
# PS1+=" \$(find . -mindepth 1 -maxdepth 1 -type f | wc -l) " # print number of files
# PS1+=" \$(find . -mindepth 1 -maxdepth 1 -type l | wc -l) " # print number of symlinks
# export PS1

# export PROMPT_COMMAND='export H1="`history 1|sed -e "s/^[\ 0-9]*//; s/[\d0\d31\d34\d39\d96\d127]*//g; s/\(.\{1,50\}\).*$/\1/g"`";history -a;echo -e "sgr0\ncnorm\nrmso"|tput -S'
# export PS1='[\j:\!]\e[0;36m \e[1;30m[\e[1;34m\u@\H\e[1;30m:\e[0;37m`tty 2>/dev/null` \e[0;32m+${SHLVL}\e[1;30m] \e[1;37m\w\e[0;37m\[\033]0;[ ${H1}... ] \w - \u@\H +$SHLVL @`tty 2>/dev/null` - [ `uptime` ]\007\]\n\[\]\$ '
BRANCH="\$(git rev-parse --abbrev-ref HEAD 2>/dev/null)"
export PS1="${FMT_BOLD}${FG_RED}[${FG_CYAN}\u${FG_WHITE}@${FG_BLUE}\H${FG_WHITE}:${FG_MAGENTA}\w${FG_WHITE}${FG_RED}]${FG_GREEN} ${BRANCH}\n\$ ${FG_WHITE}${FMT_UNBOLD}"
export PS0="\e[2 q\2"
# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Set the history file location and enable history sharing
shopt -s histappend
shopt -s histverify
HISTCONTROL=ignorecase:ignorespace:ignoredups
PROMPT_COMMAND="history -a; history -c; history -r; ${PROMPT_COMMAND}"
[[ -f ~/.bash_profile ]] && source ~/.bash_profile

shopt -s globstar

# install bash-completion package first!
source /usr/share/bash-completion/bash_completion

# keybind modes
# set -o vi # uncomment stuff in ~/.inputrc for more vim features
set -o emacs

# zsh-like autocompletion
bind 'set completion-ignore-case on'
bind 'set show-all-if-ambiguous on'
bind 'TAB:menu-complete'


loopforme(){
    while :; do
        ${READLINE_LINE}
    done
}
# bind -x '"\C-e": loopforme' # CTRL + e as in Example

# for opensuse
test -s .alias && . .alias

source <(cd "$RC_DIRECTORY" && cat -- *)
