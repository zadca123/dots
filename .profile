# -*- mode: sh; -*-

export XDG_CONFIG_HOME="${HOME}/.config"
export LS_COLORS="${LS_COLORS:-rs=0:di=01;36:ln=target:mh=00:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:mi=01;37;41:su=37;41:sg=30;43:ca=30;41:tw=30;42:ow=34;42:st=37;44:ex=01;32:*.tar=01;31:*.tgz=01;31:*.arc=01;31:*.arj=01;31:*.taz=01;31:*.lha=01;31:*.lz4=01;31:*.lzh=01;31:*.lzma=01;31:*.tlz=01;31:*.txz=01;31:*.tzo=01;31:*.t7z=01;31:*.zip=01;31:*.z=01;31:*.dz=01;31:*.gz=01;31:*.lrz=01;31:*.lz=01;31:*.lzo=01;31:*.xz=01;31:*.zst=01;31:*.tzst=01;31:*.bz2=01;31:*.bz=01;31:*.tbz=01;31:*.tbz2=01;31:*.tz=01;31:*.deb=01;31:*.rpm=01;31:*.jar=01;31:*.war=01;31:*.ear=01;31:*.sar=01;31:*.rar=01;31:*.alz=01;31:*.ace=01;31:*.zoo=01;31:*.cpio=01;31:*.7z=01;31:*.rz=01;31:*.cab=01;31:*.wim=01;31:*.swm=01;31:*.dwm=01;31:*.esd=01;31:*.jpg=01;35:*.jpeg=01;35:*.mjpg=01;35:*.mjpeg=01;35:*.gif=01;35:*.bmp=01;35:*.pbm=01;35:*.pgm=01;35:*.ppm=01;35:*.tga=01;35:*.xbm=01;35:*.xpm=01;35:*.tif=01;35:*.tiff=01;35:*.png=01;35:*.svg=01;35:*.svgz=01;35:*.mng=01;35:*.pcx=01;35:*.mov=01;35:*.mpg=01;35:*.mpeg=01;35:*.m2v=01;35:*.mkv=01;35:*.webm=01;35:*.webp=01;35:*.ogm=01;35:*.mp4=01;35:*.m4v=01;35:*.mp4v=01;35:*.vob=01;35:*.qt=01;35:*.nuv=01;35:*.wmv=01;35:*.asf=01;35:*.rm=01;35:*.rmvb=01;35:*.flc=01;35:*.avi=01;35:*.fli=01;35:*.flv=01;35:*.gl=01;35:*.dl=01;35:*.xcf=01;35:*.xwd=01;35:*.yuv=01;35:*.cgm=01;35:*.emf=01;35:*.ogv=01;35:*.ogx=01;35:*.aac=01;36:*.au=01;36:*.flac=01;36:*.m4a=01;36:*.mid=01;36:*.midi=01;36:*.mka=01;36:*.mp3=01;36:*.mpc=01;36:*.ogg=01;36:*.ra=01;36:*.wav=01;36:*.oga=01;36:*.opus=01;36:*.spx=01;36:*.xspf=01;36:}"

# RC directory, no slash at end!
export SHELL_DIRECTORY="${XDG_CONFIG_HOME}/shell"
export RC_DIRECTORY="${SHELL_DIRECTORY}/rc.d"

# Common history settings for shells
export HISTFILE="${SHELL_DIRECTORY}/history"
export SAVEHIST=30000
export HISTSIZE=30000

# custom variables
export SCREENSHOTS="${HOME}/Pictures/Screenshots"
export RECORDINGS="${HOME}/Documents/recordings"
export SCRIPTS="${HOME}/Scripts"
export WALLPAPERS="${HOME}/Pictures/wallpapers"
export TRASH="${HOME}/.local/share/Trash/files"
export BACKUPS="/media/${USER}/crucial_nvme/Backups"
export DOTFILES="${XDG_CONFIG_HOME}/dots"
export EMACS_CONFIG_DIR="${XDG_CONFIG_HOME}/emacs"
export VISUAL="emacsclient -c"
export EDITOR="emacsclient -c"
export SUDO_EDITOR="${HOME}/.local/emacs/bin/emacsclient -c" # absolute path
export ALTERNATE_EDITOR="nvim" # alternative editor when emacs is off
export TERMINAL="kitty"
export BROWSER="firefox"
export READER="zathura"
export OPEN="xdg-open"
export IMAGE="sxiv"
export VIDEO="mpv"
export PAGER="less"

# for systemd
export SYSTEMD_PAGER="" # not using pager by default
export ANSIBLE_NAVIGATOR_CONFIG="${XDG_CONFIG_HOME}/ansible/ansible-navigator.yml"

# for borgbackup
export BORG_REPO="$BACKUPS"
export BORG_PASSCOMMAND="pass misc/borg"

# for pass
export PASSWORD_STORE_DIR="${XDG_CONFIG_HOME}/password-store"
export PASSWORD_STORE_ENABLE_EXTENSIONS="true"

# for lsyncd
export LSYNCD_OPTIONS="${XDG_CONFIG_HOME}/lsyncd/lsyncd.conf"

# for fzf
FZF_DEFAULT_COMMAND="locate \$(pwd)"; export FZF_DEFAULT_COMMAND
export FZF_DEFAULT_OPTS="--layout=reverse --height 40%"

# for less
LESS="-RXi"                                      ; export LESS
LESS_TERMCAP_mb="$(printf '%b' '[1;31m')"      ; export LESS_TERMCAP_mb
LESS_TERMCAP_md="$(printf '%b' '[1;36m')"      ; export LESS_TERMCAP_md
LESS_TERMCAP_me="$(printf '%b' '[0m')"         ; export LESS_TERMCAP_me
LESS_TERMCAP_so="$(printf '%b' '[01;44;33m')"  ; export LESS_TERMCAP_so
LESS_TERMCAP_se="$(printf '%b' '[0m')"         ; export LESS_TERMCAP_se
LESS_TERMCAP_us="$(printf '%b' '[1;32m')"      ; export LESS_TERMCAP_us
LESS_TERMCAP_ue="$(printf '%b' '[0m')"         ; export LESS_TERMCAP_ue

# for qutebrowser, and qt apps in general (i think)
export QTWEBENGINE_CHROMIUM_FLAGS="--blink-settings=darkMode=4,darkModeImagePolicy=2"

# for GO / programming specific variables
export GOBIN="/opt/go/bin"
export PYENV_ROOT="/opt/pyenv"

# for nnn
export NNN_SEL="${NNN_SEL:-${XDG_CONFIG_HOME:-$HOME/.config}/nnn/.selection}"
export NNN_TRASH=1
export NNN_FIFO="/tmp/nnn.fifo"

GPG_TTY="$(tty)"; export GPG_TTY # idk, dont remember
SUDO_ASKPASS="$(which ssh-askpass)"; export SUDO_ASKPASS # for sudo -A

# adding to paths
PATH="${PATH}:$(find "$HOME" /opt -maxdepth 2 -type d -name bin -printf "%p:")"
PATH="${PATH}:/usr/sbin"
export PATH

K8S_REPO="${HOME}/Repos/work/k8s"
if [[ -d $K8S_REPO ]]; then
    KUBECONFIG="$(find "${K8S_REPO}" -name rke2.yaml -printf "%p:")"
    export KUBECONFIG
fi

# Generated for envman. Do not edit.
[ -s "$HOME/.config/envman/load.sh" ] && source "$HOME/.config/envman/load.sh"
