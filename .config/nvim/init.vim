call plug#begin('~/.config/nvim/plugged')
Plug 'junegunn/fzf'
Plug 'junegunn/fzf.vim' "recent files, search and many more
Plug 'ap/vim-css-color' "colorful hex decimals
Plug 'luochen1990/rainbow' "colorful brackets
Plug 'tpope/vim-commentary' "comment code section
Plug 'vimwiki/vimwiki' "markup language highlight
Plug 'itchyny/lightline.vim' "nicer status bar
Plug 'airblade/vim-gitgutter' "shows you lines that have been added, removed or modified
Plug 'dense-analysis/ale' "syntax checking
Plug 'sheerun/vim-polyglot' "collection of language packs
Plug 'godlygeek/tabular' "aligning text
Plug 'plasticboy/vim-markdown' "live preview
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
Plug 'tpope/vim-surround' "surround in barckets and stuff
Plug 'jceb/vim-orgmode' "orgmode in vim
Plug 'AndrewRadev/linediff.vim'
call plug#end()


set number relativenumber
set clipboard+=unnamedplus
set ignorecase " same as set ic
set nowrap
set mouse=a " enable mouse support
set hidden
set tabstop=4
set softtabstop=4
set shiftwidth=4
set expandtab
set title " name of current opened file
set nohlsearch " dont highlight search
set go=a
set encoding=utf-8
set wildcharm=<TAB> " tab as autocomplete mapping than 4 spaces
set splitbelow splitright " Splits open at the bottom and right
set wildmode=longest,list,full " Enable autocompletion

" colorscheme industry
syntax on
syntax enable on
filetype plugin indent on

" undo
if has('persistent_undo')                     "check if your vim version supports it
    set undofile                              "turn on the feature
    silent !mkdir -p "${HOME}/.config/nvim/undo"  "create directory if not exist
    set undodir="${HOME}/.config/nvim/undo"       "directory where the undo files will be stored
endif

let g:rainbow_active = 1 "set to 0 if you want to enable it later via :RainbowToggle

" highlight line in insert mode
autocmd InsertEnter * set cul
autocmd InsertLeave * set nocul

" save and load folds
augroup remember_folds
    autocmd!
    autocmd BufWinLeave * silent! mkview
    autocmd BufWinEnter * silent! loadview
augroup END

" autocomplete brackets
inoremap " ""<left>
inoremap ' ''<left>
inoremap ( ()<left>
inoremap [ []<left>
inoremap { {}<left>

" Automatically deletes all trailing whitespace and newlines at end of file on save.
autocmd BufWritePre * %s/\s\+$//e
autocmd BufWritePre * %s/\n\+\%$//e
autocmd BufWritePre *.[ch] %s/\%$/\r/e

" Save file as sudo on files that require root permission
cnoremap w!! execute 'silent! write !sudo tee % >/dev/null' <bar> edit!

" Disables automatic commenting on newline:
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

" what does it do?
nnoremap c "_c

" set default folder to buffer folder
autocmd BufEnter * silent! lcd %:p:h

" some emacs keybindins
cnoremap <C-g> <C-c>
nnoremap <C-x><C-s> :w<CR>
nnoremap <C-x><C-w> :write<SPACE><TAB>
nnoremap <C-x>0 :quit<CR>
nnoremap <C-x>1 :only<CR>
nnoremap <C-x>2 :split<CR>
nnoremap <C-x>3 :vsplit<CR>
nnoremap <C-x>d :Explore<SPACE><TAB>
nnoremap <C-x><C-f> :edit<SPACE><TAB>
nnoremap <C-x>k :b#<bar>bd#<CR>
nnoremap <C-x>b :buffers<CR>:buffer<SPACE>
nnoremap <C-x>xt :set wrap!<CR>
nnoremap <C-x>y :%yank<CR>
nnoremap <C-x><C-e> :exec '!'.getline('.')<CR>

nnoremap <C-c><C-e> :!detach.sh emacsclient -a '' -c %<CR><CR>
nnoremap <C-c>e :e $MYVIMRC<CR>
nnoremap <C-c>r :source $MYVIMRC<CR>
nnoremap <C-c>ot :!<SPACE>
nnoremap <C-c>oT :call termopen($SHELL, {'curwin' : 1})<CR>
nnoremap <C-c>oe :!detach.sh kitty -- tmux<CR><CR>
nnoremap <C-c><C-c> :!compile.sh %<CR>

tnoremap <esc> <C-\><C-n><CR>

" Mappings to move lines
nnoremap <A-j> :m .+1<CR>==
nnoremap <A-k> :m .-2<CR>==
inoremap <A-j> <Esc>:m .+1<CR>==gi
inoremap <A-k> <Esc>:m .-2<CR>==gi
vnoremap <A-j> :m '>+1<CR>gv=gv
vnoremap <A-k> :m '<-2<CR>gv=gv

nnoremap <SPACE> <Nop>
let mapleader=" " "leader as space

" fzf keybindings
nnoremap <Leader>f :Files<ESC>
nnoremap <Leader>b :Buffers<ESC>
nnoremap <C-x>pf :GFiles<ESC>
